pipeline {
    agent any

    environment {
        DOCKER_CREDENTIALS_ID = 'tien-dockerhub' // ID thông tin đăng nhập Docker Hub
        DOCKER_HUB_REPO = 'dvtien0805/banruou' // Repository Docker Hub
        MYSQL_ROOT_PASSWORD = '123456' // Mật khẩu root của MySQL
        MYSQL_DATABASE = 'banruou' // Tên cơ sở dữ liệu MySQL
        MYSQL_IMAGE = 'mysql:8.0'  // Image MySQL
        REMOTE_SERVER_IP_QA = 'ec2-54-151-164-64.ap-southeast-1.compute.amazonaws.com' // Địa chỉ IP của server QA
        SSH_CREDENTIALS_ID = 'my-key'  // ID thông tin đăng nhập SSH
    }

    stages {
        stage('Build Backend Imag') {
            steps {
                script {
                    docker.build('be_banruou', './be') // Build image backend từ thư mục ./be
                }
            }
        }

        stage('Build Frontend Imag') {
            steps {
                script {
                    docker.build('fe_banruou', './fev2') // Build image frontend từ thư mục ./fev2
                }
            }
        }
        
        stage('Setup Environment') {
            steps {
                script {                 
                    sh 'docker stop mysql || true' // Dừng container MySQL nếu đang chạy
                    sh 'docker rm -f mysql || true' // Xóa container MySQL nếu tồn tại
                    sh 'docker stop be_banruou || true' // Dừng container backend nếu đang chạy
                    sh 'docker rm -f be_banruou || true' // Xóa container backend nếu tồn tại
                    sh 'docker stop fev2_banruou || true' // Dừng container frontend nếu đang chạy
                    sh 'docker rm -f fev2_banruou || true' // Xóa container frontend nếu tồn tại
                }
            }
        }

        stage('Start MySQL Container') {
            steps {
                script {
                    sh """
                        docker run -d --name mysql  \
                        -e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD} \
                        -e MYSQL_DATABASE=${MYSQL_DATABASE} \
                        ${MYSQL_IMAGE}
                    """ // Khởi động container MySQL
                    sh 'sleep 10' // Đợi MySQL khởi động
                }
            }
        }
        
        stage('Run Backend') {
            steps {
                script {
                    sh "docker run -d --name be_banruou  \
                        -p 8081:80 be_banruou" // Khởi động container backend
                }
            }
        }
        
        stage('Run Frontend') {
            steps {
                script {
                    sh "docker run -d --name fev2_banruou  \
                        -p 80:80 fe_banruou" // Khởi động container frontend
                }
            }
        }
        stage('Push Images to Docker Hub') {
            steps {
                script {
                    docker.withRegistry("https://index.docker.io/v1/", DOCKER_CREDENTIALS_ID) {
                        sh 'docker tag be_banruou ${DOCKER_HUB_REPO}:be_latest' // Tag image backend với tag be_latest
                        sh 'docker tag fe_banruou ${DOCKER_HUB_REPO}:fe_latest' // Tag image frontend với tag fe_latest
                    
                        sh 'docker push ${DOCKER_HUB_REPO}:be_latest' // Push image backend lên Docker Hub
                        sh 'docker push ${DOCKER_HUB_REPO}:fe_latest' // Push image frontend lên Docker Hub
                    }
                }
            }
        }
        stage('Deploy to QA/Staging') {
            steps {
                script {
                    sshagent([SSH_CREDENTIALS_ID]) {
                        sh """
                            ssh -o StrictHostKeyChecking=no ubuntu@${REMOTE_SERVER_IP_QA} '
                            # Pull các image mới nhất
                            sudo docker pull ${DOCKER_HUB_REPO}:be_latest
                            sudo docker pull ${DOCKER_HUB_REPO}:fe_latest

                            # Dừng và xóa các container hiện tại
                            sudo docker stop mysql || true
                            sudo docker rm -f mysql || true
                            sudo docker stop be_banruou || true
                            sudo docker rm -f be_banruou || true
                            sudo docker stop fev2_banruou || true
                            sudo docker rm -f fev2_banruou || true

                            # Khởi động container MySQL
                            sudo docker run -d --name mysql  \
                            -e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD} \
                            -e MYSQL_DATABASE=${MYSQL_DATABASE} \
                            ${MYSQL_IMAGE}
                            
                            # Đợi MySQL sẵn sàng
                            while ! sudo docker exec mysql mysqladmin ping -h localhost -u root -p${MYSQL_ROOT_PASSWORD}; do
                            sleep 1
                            done

                            # Khởi động container backend
                            sudo docker run -d --name be_banruou  \
                            -p 8081:80 ${DOCKER_HUB_REPO}:be_latest

                            # Khởi động container frontend
                            sudo docker run -d --name fev2_banruou \
                            -p 80:80 ${DOCKER_HUB_REPO}:fe_latest
                            '
                        """ // Kết nối SSH và triển khai ứng dụng lên server QA
                    }
                }
            }
        }
    }
}
